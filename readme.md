# Quantum complexity

## The report

1. Install TeX Live (full package).

2. Compile the report.
```
latex complexity.tex
bibtex complexity.aux
makeglossaries complexity 
pdflatex complexity.tex mupdf
```
