\section{Der Komplexitätsbegriff} 

  In der theoretischen Informatik werden \ua Probleme nach Ihrer Komplexität analysiert.
  Die Komplexität eines Problems gibt auf abstrakte Weise an, wie schwer es ist, dieses Problem zu lösen.

  \subsection{Die Laufzeit}

    Um das Konzept der Komplexität zu verstehen, bedarf es zunächst an einer Definition der Laufzeit $t$.
    Die Laufzeit ist eine Kenngröße von jedem Algorithmus welche angibt, wie viele Rechenschritte notwendig sind, um diesen Algorithmus vollständig auszuführen.
    Man nutzt hierfür keine absolute Zeit, da diese auf verschiedenen Computern stark variieren kann.
    Die Anzahl an Rechenschritten hingegen sind unabhängig von dem konkreten Rechner. \cite[68\psq]{homeister2018}
  
    Häufig variiert die Laufzeit auch in Bezug auf eine oder mehrere Eingangsgrößen $n$.
    Ist dies der Fall, so lässt sich die Laufzeit als Formel darstellen.

  \subsection{Die Komplexität}

    Die Betrachtung der Laufzeit ist vor allem in der Praxis sinnvoll.
    Konkrete Laufzeitformeln können jedoch schnell sehr umfangreich werden.
    Um lediglich das Skalierungsverhalten eines Programmes zu untersuchen ist dieser Umfang eher von geringem Interesse.
    Hierzu wird eine weitere Größe in Form der Komplexität eingeführt.
    Die Komplexität umfasst damit nur die einflussreichste Komponente der Laufzeit ohne Vorfaktoren und Konstanten.
    Man schreibt $\mathcal{O}(c_n)$, wobei $c_n$ die Formel zur Komplexität in Abhängigkeit von $n$ darstellt. \cite[70\psqq]{homeister2018}

    \begin{equation}
      \label{eq:to}
      t(n) = a \cdot n^2 + b \cdot n + c = \mathcal{O}(n^2)
    \end{equation}

    Die Formel \eqref{eq:to} gibt einen beispielhaften Zusammenhang zwischen Laufzeit und Komplexität wieder.
    Es finden sich in der Praxis immer wieder auftretende Komplexitäten.
    Einige von Ihnen sind in dem folgenden Diagramm \ref{fig:complexityComp} exemplarisch dargestellt.
    Deutlich erkennbar ist, wie für effiziente Komplexitäten wie $\mathcal{O}(log(n))$ auch noch für hohe $n$ die Laufzeit $t$ vergleichbar gering bleibt.
    Hohe Komplexitäten wie $\mathcal{O}(k^n)$ sind jedoch nur für relativ kleine $n$ mit akzeptabler Laufzeit $t$ ausführbar.

    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}
        \begin{axis}[
          xmin = 0, xmax = 1000,
          ymin = 0, ymax = 1000,
          xlabel = {$n$},
          ylabel = {$t$},
          axis lines=left,
          xtick=\empty,
          ytick=\empty,
          width = \textwidth,
          height = 0.4\textwidth,
          legend cell align = {left},
          legend pos = north east
        ]

        \addplot[blue, domain = 0:1000] {50 * ln(x+50) - 200};
        \addplot[red, domain = 0:1000] {0.5*x};
        \addplot[green, domain = 0:1000] {0.25*x*ln(0.1*x)};
        \addplot[purple, domain = 0:250] {0.02*x^2};
        \addplot[teal, domain = 0:65] {0.01*exp(0.1*(x+50))};

        \legend{
          $\mathcal{O}(log(n))$,
          $\mathcal{O}(n)$,
          $\mathcal{O}(n \cdot log(n))$,
          $\mathcal{O}(n^2)$,
          $\mathcal{O}(k^n)$
        }

        \end{axis}

      \end{tikzpicture}

      \caption{Vergleich gängiger Komplexitäten}
      \label{fig:complexityComp}

    \end{figure}

    Es wird zudem zwischen der Komplexität eines Algorithmus und eines Problems unterschieden.
    Da ein Problem von mehreren Algorithmen gelöst werden kann, bezeichnet man als Komplexität eines Problems die Komplexität des effizientesten Algorithmus, welcher dieses Problem löst. \cite[101]{homeister2018}

\section{Problemarten}

  Probleme lassen sich in einige essenzielle Kategorien unterteilen.
  Um diese Unterteilungen vorzunehmen, sehen wir zunächst ein Problem als die Suche nach einer bestimmten Abbildung von einem Definitionsbereich $d$ auf einen Bildbereich $b$.
  Die Bereiche $d$ und $b$ sind durch das Problem vorgegeben.
  Eine Lösung des Problems stellt ein Algorithmus $A$ dar, welche die gesuchte Transformation von $d$ nach $b$ implementiert.
  Probleme nach dieser allgemeinen Definition bezeichnet man auch als Berechnungsprobleme.

  Berechnungsprobleme lassen sich durch Einschränkungen des Definitions- oder Bildbereiches weiter unterteilen.
  Die wichtigste für die Komplexitätstheorie Problemart stellen Entscheidungsprobleme $E$ dar.
  Sie haben einen Bildbereich $b = {0;1}$.
  Dadurch stellen Sie eine Ja-/Nein-Antwort dar.
  Man fragt bei der Problemstellung also, ob eine gewisse Eingabe eine Bedingung erfüllt oder nicht. \cite[103]{homeister2018}

  Ebenfalls von Interesse sind Optimierungsprobleme $O$.
  Hier sucht man nach dem Minimum \bzw Maximum einer Kostenfunktion über alle Lösungen eines anderen Problems.

\section{Klassische Komplexitätsklassen}

  Um die theoretische Lösbarkeit von Problemen zu ermitteln, definiert man Komplexitätsklassen.
  Diese Klassen bilden Mengen, welche zueinander in Relation stehen können.
  Mit Hilfe dieser Relationen lassen sich dann Aussagen treffen, welche Probleme auf welchen Arten von Rechnern zu lösen sind.
  Damit bilden Komplexitätsklassen ein sehr hilfreiches, abstraktes Mittel der theoretischen Informatik.

  \subsection{Die Komplexitätsklasse P}

    \begin{definition}
      Zur Menge \gls{p} gehören alle Entscheidungsprobleme $E$ für die ein Algorithmus $A$ existiert, welcher $E$ für alle Eingaben innerhalb von $\mathcal{O}(n^k)$ mit konstanten $k$ löst.
    \end{definition}

    Die grundlegendste Komplexitätsklasse bildet die Menge \gls{p}.
    Wir sehen Algorithmen, welcher ein Entscheidungsproblem $E$ in Polynomialzeit löst per Definition als effizient an.
    Eine Beschränkung auf $\mathcal{O}(n^k)$ mag zunächst willkürlich erscheinen, da uns für hohe $k$ definitiv keine praktisch skalierbare Lösung des Problems vorliegt.
    In der Praxis hat sich jedoch gezeigt, dass für Probleme in \gls{p} oft Algorithmen mit Komplexitäten geringer Ordnung finden.
    Demnach sind für Probleme in \gls{p} also Algorithmen bekannt, welche auf klassischen Rechnern effizient durchführbar sind. \cite[102]{homeister2018}
    
    \begin{equation}
      \label{eq:p}
      \mathcal{O}_{\gls{p}12}(\mathcal{O}_{\gls{p}1} \cdot \mathcal{O}_{\gls{p}2}) = \mathcal{O}(n^k)
    \end{equation}

    Die Klasse \gls{p} hat zudem noch weitere hilfreiche Eigenschaft.
    Rufen wir innerhalb eines \gls{p}-Algorithmus einen anderen \gls{p}-Algorithmus auf, so gilt für den ungünstigsten Fall, dass die Komplexitäten beider Algorithmen miteinander multipliziert werden. \eqref{eq:p}
    Da die Multiplikation von zwei Polynomen wiederum ein Polynom ergibt, muss auch der neue Algorithmus in \gls{p} liegen.
    Wir können Probleme in \gls{p} also beliebig miteinander kombinieren und erhalten ebenfalls ein Problem in \gls{p}. \cite[102\psq]{homeister2018}

  \subsection{Die Komplexitätsklasse NP}

    \begin{definition}
      Zur Menge \gls{np} gehören alle Entscheidungsprobleme $E$, wenn eine Verifikationsfunktion $f(x,y)$ mit polynomialer Komplexität existiert, sodass nur für alle Eingaben $x$ mit $E(x) = 1$ ein Zertifikat $y$ existiert mit $f(x,y) = 1$.
    \end{definition}

    Die Komplexitätsklasse \gls{np} beschreibt alle Entscheidungsprobleme, bei denen für die eine mögliche Eingabe $x$ leicht zu überprüfen ist, ob diese Eingabe das Problem löst.
    Es existiert also eine Verifikationsfunktion $f$, welche in \gls{p} liegt. \cite[104]{homeister2018}
    Die Bezeichnung nicht-deterministische Polynomialzeit hat ihren Ursprung darin, dass ein nicht-deterministischer Rechner Probleme in \gls{np} in polynomialer Zeit lösen kann.
    Es handelt sich hierbei um ein theoretisches Werkzeug.
    Der nicht-deterministische Rechner führt hierzu die Verifikationsfunktion $f$ für alle möglichen Eingaben $x$ gleichzeitig aus \bzw wählt zufällig eine korrekte Eingabe aus.
    Auf herkömmlichen Rechnern ist dies nicht möglich.
    Hier ist für die Komplexität des Problems dann maßgeblich das möglicherweise exponentielle Wachstum der Eingabemenge zur Eingangsgröße $n$ relevant.
    \Idr sind \gls{np}-Algorithmen auf klassischen Rechnern also nicht lösbar.

    \begin{equation}
      \label{eq:pnp}
      \gls{p} \subseteq \gls{np}
    \end{equation}

    Jedoch ist die Klasse \gls{p} selbst in \gls{np} enthalten, da man als Verifikationsfunktion $f$ des Problems den bekannten \gls{p}-Lösungsalgorithmus verwenden und dessen Resultat mit der Eingabe vergleichen kann. \eqref{eq:pnp}

    \subsubsection{Polynomialzeitreduktion}

      Wir haben bereits festgestellt, dass Probleme in \gls{np} auf klassischen Rechnern möglicherweise eine exponentielle Komplexität besitzen.
      Es stellt sich jedoch die Frage, ob wir diese exponentiellen Probleme noch weiter unterteilen können.
      Für uns ist es vor allem interessant, wenn eine Problemstellung in polynomialer Zeit in eine andere umgeformt werden kann, dessen Lösung wir möglicherweise kennen.
      Hierzu ist es sinnvoll einen Vergleichsoperator zwischen zwei Problemen zu definieren.

      \begin{definition}
        Es gilt für $E_0$ und $E_1$ aus \gls{np} der Vergleichsoperator $E_0 \leq_p E1$, wenn es einen Algorithmus $A$ in \gls{p} gibt, welcher durch eine Funktion $f$ die Eingaben aus $E_0$ auf die Eingaben von $E_1$ abbildet, sodass $E_0(x) = E_1(f(x))$. $A$ heißt Polynomialzeitreduktion von $E_0$ auf $E_1$.
      \end{definition}

      Wir sagen für $E_0 \leq_p E_1$, dass $E_1$ mindestens so schwer ist, wie $E_0$.
      Mit Hilfe dieser Definition haben wir \gls{np} weiter unterteilt, und können damit gezielter Aussagen treffen wie schwer ein Problem ist. \cite[114\psqq]{homeister2018}
      Zudem ermöglicht uns dieses Werkzeug polynomiale Algorithmen für ganze Klassen an Problemen zu finden.
      Denn wenn wir eine Polynomialzeitreduktion auf ein Problem in \gls{p} durchführen können, handelt es sich dabei lediglich um eine Kombination von \gls{p}-Algorithmen, welche wie behandelt selbst ein \gls{p}-Algorithmus ist.
      Damit liegt auch unser Ausgangsproblem in \gls{p}.
      Es ist anzumerken, dass die obige Definition nur für Probleme aus \gls{np} gilt.
      Dies beschränkt allerdings nicht die Existenz einer Polynomialzeitreduktion nur für Probleme außerhalb von \gls{np} ein.

  \subsection{Die Komplexitätsklasse NP-schwer}

    \begin{definition}
      Zur Menge \gls{np}-schwer gehören alle Probleme $P$, welche mindestens so schwer sind wie alle Entscheidungsprobleme $E$ aus \gls{np}: $E \leq_p P$.
    \end{definition}

    Mit der Polynomialzeitreduktion lässt sich nun eine weitere Komplexitätsklasse definieren.
    Uns interessieren für die Klasse \gls{np}-schwer \bzw \gls{np}-hard alle Probleme, welche mindestens so schwer sind wie Probleme aus \gls{np}. \cite[116]{homeister2018}
    Hierzu gehören nun auch Probleme, welche sich \ggf nicht einmal effizient verifizieren lassen.
    Ebenso enthält \gls{np}-schwer auch nicht nur Entscheidungsprobleme wie \gls{p} und \gls{np}, da Polynomialzeitreduktionen auch für andere Probleme existieren.

  \subsection{Die Komplexitätsklasse NP-vollständig}

    \begin{definition}
      Die Menge \gls{np}-vollständig ist die Schnittmenge aus \gls{np} und \gls{np}-schwer: $\text{\gls{np}-vollständig} = \text{\gls{np}} \cap \text{\gls{np}-schwer}$.
    \end{definition}

    Durch die Definition der Menge \gls{np}-schwer bildet sich auch eine Schnittmenge zwischen \gls{np} und \gls{np}-schwer, da die schwersten Probleme in \gls{np} logischerweise selbst mindestens so schwer sind wie alle Probleme aus \gls{np}.
    Wir bezeichnen diese Probleme als \gls{np}-vollständig oder \gls{np}-complete.
    Diese Komplexitätsklasse ist von großer Bedeutung, da jedes Problem in \gls{np} auf ein Problem in \gls{np}-vollständig reduziert werden kann.
    Findet man also einen polynomialen Algorithmus für ein Problem in \gls{np}-vollständig, so ist auch jedes andere Problem in \gls{np} in polynomialer Zeit lösbar.
    Es würde also gelten, dass es gar keinen Unterschied zwischen den Mengen \gls{p} und \gls{np} gibt \bzw $\text{\gls{p}} = \text{\gls{np}}$.
    In diesem Fall wäre auch die Klasse \gls{np}-vollständig hinfällig, da nun jedes Problem in \gls{np} und somit auch \gls{p} aufeinander reduzierbar und daher gleich schwer ist.
    Es gilt folglich auch $\text{\gls{p}} = \text{\gls{np}} = \text{\gls{np}-vollständig}$.
    Welcher der beiden Aussagen $\text{\gls{p}} = \text{\gls{np}}$ und $\text{\gls{p}} \neq \text{\gls{np}}$ gilt, ist eine der wohl berühmtesten ungelösten Fragen der theoretischen Informatik und ist Teil der sieben Millennium-Probleme. \cite{clay2021}
    Auch wenn uns bisher kein Beweis für jene der Szenarien bekannt ist, gehen viele Mathematiker davon aus, dass $\text{\gls{p}} \neq \text{\gls{np}}$ gilt. \cite[117\psq]{homeister2018}
    Die Annahme $\text{\gls{p}} = \text{\gls{np}}$ führt nämlich zu einigen mit unserem Verständnis schwer zu vereinbarenden Schlüssen.
    So wäre \bspw in einem abstrakten Sinne durch die Formulierung eines Problems auch schon die Lösung bekannt.

    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}[node distance = 30pt]

        \tikzstyle{every node}=[font=\scriptsize]
  
        % p != np
        \node[draw, ellipse, minimum width=30pt, minimum height=30pt, anchor=south] at (0,0){$\text{\gls{p}}$};
        \node[draw, ellipse, minimum width=125pt, minimum height=90pt, anchor=south] at (0,0){$\text{\gls{np}}$};
        \node[draw, ellipse, minimum width=125pt, minimum height=90pt, anchor=south] at (0,2){};
        \node at (0,4) {$\text{\gls{np}-schwer}$};
        \node at (0,2.5) {$\text{\gls{np}-vollständig}$};
        \node[anchor=east] at (-1.5,5) {$\text{\gls{p}} \neq \text{\gls{np}}$};
        
        % p = np
        \node[draw, ellipse, minimum width=90pt, minimum height=90pt, anchor=south, text width=55pt,align=center] at (6,0){$\text{\gls{p}}$\\$\text{\gls{np}}$\\$\text{\gls{np}-vollständig}$};
        \node[draw, ellipse, minimum width=125pt, minimum height=150pt, anchor=south] at (6,0){};
        \node at (6,4) {$\text{\gls{np}-schwer}$};
        \node[anchor=east] at (4.5,5) {$\text{\gls{p}} = \text{\gls{np}}$};
      
      \end{tikzpicture}
  
      \caption{Beziehungen zwischen klassischen Komplexitätsklassen für die Fälle $\text{\gls{p}} \neq \text{\gls{np}}$ und $\text{\gls{p}} = \text{\gls{np}}$}
      \label{fig:kRel}
  
    \end{figure}

\section{Probabilistische Komplexitätsklassen}

  Bisher haben wir die Komplexität von Algorithmen untersucht, welche ein deterministisches und folglich verlässliches Ergebnis liefern.
  Es existieren jedoch auch Algorithmen, welche nur zu einer gewissen Wahrscheinlichkeit das korrekte Ergebnis zurückgeben.
  Hierfür bedarf es an gesonderten Komplexitätsklassen, welche diese Eigenheit berücksichtigen.

  \subsection{Die Komplexitätsklasse RP}

    \begin{definition}
      Zur Menge \gls{rp} gehören alle Entscheidungsprobleme $E$ für die ein randomisierter Algorithmus $A$ in \gls{p} existiert, für den gilt: wenn $A(x) = 0$ dann ist auch $E(x) = 0$ und wenn $A(x) = 1$ so ist $E(x) = 1$ mit einer Wahrscheinlichkeit $p$ größer $\frac{1}{2}$.
    \end{definition}
    
    Die grundlegendste probabilistische Komplexitätsklasse bildet \gls{rp}.
    Sie beinhaltet Probleme, welche sich durch einen polynomialen Algorithmus $A$ lösen lassen, welcher das Ergebnis nicht-deterministisch errät.
    Liefert der Algorithmus das Ergebnis $0$, so besteht komplette Gewissheit über die Lösung.
    Gibt der Algorithmus jedoch $1$ zurück bedeutet dies nur zu einer gewissen Wahrscheinlichkeit, dass auch die Lösung des Problems tatsächlich $1$ ist.
    Dies bezeichnet man als einseitigen Fehler. \cite[110]{homeister2018}
    Um die Fehlerwahrscheinlichkeit $\bar{p}$ zu verbessern, kann man den Algorithmus mehrmals durchführen.
    Mit jeder Durchführung multipliziert sich hierbei die Gesamtfehlerwahrscheinlichkeit $\bar{p}_{gesamt}$ in dem Fall $A(x) = 1$ um $\bar{p}$ \bzw wird bei $A(x) = 0$ gänzlich eliminiert.

    \begin{equation}
      \label{eq:rpp}
      \bar{p}_{gesamt} = (\bar{p})^k
    \end{equation}

    Mit $k$ Wiederholungen lässt sich die Gewissheit über das Ergebnis also exponentiell steigern. \eqref{eq:rpp}
    Dies hat keine Auswirkung auf die Komplexität von $A$, da es sich bei $k$ lediglich um einen Vorfaktor in der Laufzeitberechnung handelt und somit nicht zu berücksichtigen ist.
    Den Vorgang nennt man auch probabilty amplification, da die Wahrscheinlichkeit das richtige Ergebnis zu erhalten verstärkt wird. \cite[110\psq]{homeister2018}

    \begin{equation}
      \label{eq:rpr}
      \text{\gls{p}} \subseteq \text{\gls{rp}} \subseteq \text{\gls{np}}
    \end{equation}

    Die Klasse \gls{rp} ist sowohl eine Obermenge zu \gls{p} als auch eine Teilmenge zu \gls{np}. \eqref{eq:rpr}
    Um die Relation $\text{\gls{p}} \subseteq \text{\gls{rp}}$ zu begründen, kann man ein \gls{p}-Problem lediglich ein \gls{rp} Problem mit einer Fehlerwahrscheinlichkeit von 0 ansehen.
    Für $\text{\gls{rp}} \subseteq \text{\gls{np}}$ betrachtet man die Anzahl an Zertifikaten $n_y$, welche eine mögliche Lösung eines \gls{rp} \bzw \gls{np}-Problems verifizieren.
    Dies ist äquivalent zu der Anzahl an möglichen Rechenwegen.
    Damit die Bedingung $p > \frac{1}{2}$ für ein \gls{rp}-Problem erfüllt wird, muss $n_y$ größer als die Hälfte der Anzahl der möglichen Eingaben sein.
    Für \gls{np}-Probleme muss hingegen nur ein einziges Zertifikat existieren, damit das Problem lösbar ist.
    Somit ist die Bedingung an ein \gls{rp}-Problem schärfer. \cite[113]{homeister2018}

  \subsection{Die Komplexitätsklasse BPP}

    \begin{definition}
      Zur Menge \gls{bpp} gehören alle Entscheidungsprobleme $E$ für die ein randomisierter Algorithmus $A$ in \gls{p} existiert, welcher das korrekte Ergebnis mit einer Wahrscheinlichkeit $p$ größer $\frac{1}{2}$ liefert.
    \end{definition}

    Neben der Klasse \gls{rp} mit einem einseitigen Fehler wird auch eine Komplexitätsklasse mit zweiseitigem Fehler benötigt.
    Algorithmen für \gls{bpp}-Probleme besitzen einen zweiseitigen Fehler.
    Der Fall $A(x) = 0$ bildet nun keine Abbruchbedinung mehr.
    Es bleibt hierbei also unabhängig von der Ausgabe und Wiederholungen ein Restfehler. \cite[112]{homeister2018}

    \begin{equation}
      \label{eq:bppp}
      \bar{p}_{gesamt} = (\bar{p})^{\ceil{\frac{k}{2}} + 1}
    \end{equation}

    Ähnlich zu \gls{rp} lässt sich auch bei \gls{bpp}-Problemen die Fehlerwahrscheinlichkeit mit mehreren Durchläufen reduzieren. \eqref{eq:bppp}
    Hierbei entscheidet man sich allerdings für das Ergebnis, welches bei den Iterationen am häufigsten aufgetreten ist.
    Diesen Vorgang bezeichnet man als Mehrheitsvotum. \cite[112]{homeister2018}

    \begin{equation}
      \label{eq:bppr}
      \text{\gls{rp}} \subseteq \text{\gls{bpp}}
    \end{equation}

    Die Klasse \gls{bpp} steht zunächst nur mit \gls{rp} in Relation.
    \gls{bpp} bildet eine Übermenge zu \gls{rp}. \eqref{eq:bppr}
    Dies lässt sich erneut einfach damit begründen, dass ein \gls{rp}-Problem auch als \gls{bpp}-Problem zu verstehen ist, bei dem eine Fehlerwahrscheinlichkeit bei $0$ liegt.
    Zu \gls{np} ist nun aber keine Beziehung mehr vorhanden. \cite[113]{homeister2018}
    Für uns sind diese Relationen von großer Bedeutung, da wir nun auch Probleme als effizient lösbar betrachten können, welche außerhalb von \gls{p} und sogar \gls{np} liegen.
  
    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}[node distance = 30pt]

        \tikzstyle{every node}=[font=\scriptsize]
  
        \node[draw, ellipse, minimum width=30pt, minimum height=30pt, anchor=south] at (0,0){$\text{\gls{p}}$};
        \node[draw, ellipse, minimum width=60pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (0,1.5) {$\text{\gls{rp}}$};
        \node[draw, ellipse, minimum width=90pt, minimum height=100pt, anchor=south] at (0,0){};
        \node at (0,3) {$\text{\gls{np}}$};
        \node[draw, ellipse, minimum width=210pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (2.5,1) {$\text{\gls{bpp}}$};
      
      \end{tikzpicture}
  
      \caption{Beziehungen zwischen klassischen und probabilistischen Komplexitätsklassen}
      \label{fig:pRel}
  
    \end{figure}

\section{Quantenkomplexitätsklassen}

  Quantencomputer können Probleme lösen, welche nach unserer Kenntnis auf klassischen Rechnern nicht effizient berechenbar sind.
  Es liegt also nahe, eine Komplexitätsklasse für diese Probleme einzuführen.

  \subsection{Die Komplexitätsklasse BQP}

    \begin{definition}
      Zur Menge \gls{bqp} gehören alle Entscheidungsprobleme $E$ für die ein durch Quantenschaltkreise berechenbarer Algorithmus $A$ in \gls{p} existiert, welcher das korrekte Ergebnis mit einer Wahrscheinlichkeit größer $\frac{1}{2}$ liefert.
    \end{definition}

    Die Definition der Komplexitätsklasse \gls{bqp} ähnelt zunächst stark \gls{bpp}.
    Dies ist nicht abwegig, da Quantenalgorithmen oft Probleme mit einem gewissen Fehler berechnen können.
    Es gelten also die gleichen Gesetze für die probabilty amplification wie bei \gls{bpp}. \eqref{eq:bppp}

    \begin{equation}
      \label{eq:bqpr}
      \text{\gls{bpp}} \subseteq \text{\gls{bqp}}
    \end{equation}

    \gls{bqp} ist eine Übermenge zu \gls{bpp}, da durch Quantenschaltkreise auch jedes klassische Problem gelöst werden kann und zudem das Messen eines Quantenbits in einer Superposition als ein Zufallsgenerator für die probabilistische Charakteristik von \gls{bpp}-Algorithmen genutzt werden kann. \eqref{eq:bqpr}
    Es lassen sich also alle \gls{bpp}-Probleme auch auf Quantencomputern lösen und sind folglich Teil von \gls{bqp}. \cite[118\psq]{homeister2018}
    Ähnlich wie bei dem Problem $\text{\gls{p}} \stackrel{?}{=} \text{\gls{np}}$ stellt sich auch hier die Frage, ob $\text{\gls{bpp}} = \text{\gls{bqp}}$ gilt.
    Auch hier wird davon ausgegangen, dass $\text{\gls{bpp}} \neq \text{\gls{bqp}}$ gilt, da sonst quantenmechanische Effekte wie Superpositionen, Verschränkung und Interferenz auch in der klassischen Welt existieren oder sich zumindest auf klassischen Computern effizient simulieren lassen müssten.
    Darauf deutet nach aktuellem Stand jedoch nichts darauf hin.
    So gibt es diverse Algorithmen, wie \bspw der Deutsch-Josza Algorithmus oder der Simon Algorithmus, welche diese Effekte nutzen, um Probleme mit exponentieller Laufzeit auf Quantencomputern effizient zu lösen.
    Trotzdem ist keine der beiden Aussagen $\text{\gls{bpp}} = \text{\gls{bqp}}$ und $\text{\gls{bpp}} \neq \text{\gls{bqp}}$ bisher bewiesen. \cite[120]{homeister2018}

    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}[node distance = 30pt]

        \tikzstyle{every node}=[font=\scriptsize]
  
        \node[draw, ellipse, minimum width=30pt, minimum height=30pt, anchor=south] at (0,0){$\text{\gls{p}}$};
        \node[draw, ellipse, minimum width=60pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (0,1.5) {$\text{\gls{rp}}$};
        \node[draw, ellipse, minimum width=90pt, minimum height=100pt, anchor=south] at (0,0){};
        \node at (0,3) {$\text{\gls{np}}$};
        \node[draw, ellipse, minimum width=210pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (2.5,1) {$\text{\gls{bpp}}$};
        \node[draw, ellipse, minimum width=330pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (4.5,1) {$\text{\gls{bqp}}$};
      
      \end{tikzpicture}
  
      \caption{Beziehungen von \gls{bqp} zu anderen Komplexitätsklassen}
      \label{fig:qRel}
  
    \end{figure}

\section{Speicherkomplexitätsklassen}

  Bisher haben wir Komplexität nur in Abhängigkeit von der Laufzeit betrachtet.
  Es kann jedoch auch sinnvoll sein, den Speicherverbrauch eines Algorithmus in Relation zu einer Eingangsgröße zu betrachten.

  \subsection{Die Komplexitätsklasse PSPACE}

    \begin{definition}
      Zur Menge \gls{pspace} gehören alle Probleme $P$, für die ein deterministischer Algorithmus $A$ existiert, welcher polynomial Speicher verbraucht.
    \end{definition}

    Die grundlegendste Speicherkomplexitätsklasse ist \gls{pspace}.
    Sie umfasst alle Probleme, welche mit einem polynomialen Speicherverbrauch lösbar sind. \cite[120]{homeister2018}
    Wie wir sehen werden, umfasst das erstaunlich viele Probleme.

    \begin{equation}
      \label{eq:pspacer1}
      \text{\gls{p}} \subseteq \text{\gls{np}} \subseteq \text{\gls{pspace}}
    \end{equation}

    \begin{equation}
      \label{eq:pspacer2}
      \text{\gls{p}} \subseteq \text{\gls{bqp}} \subseteq \text{\gls{pspace}}
    \end{equation}

    Zunächst ist \gls{pspace} eine Übermenge zu \gls{p}.
    Dieser Zusammenhang kann dadurch begründet werden, dass für die Belegung einer Speicherzelle auch immer eine Rechenoperation notwendig ist.
    Somit kann der Speicherverbrauch also nie stärker ansteigen als die Laufzeit und \gls{p}-Probleme müssen daher auch polynomial Speicher nutzen.
    Erstaunlicherweise ist \gls{pspace} auch eine Übermenge zu \gls{np}.
    Da wir theoretisch ein \gls{np}-Problem durch sequenzielles Ausführen aller Verifikationsfunktionen in \gls{p}, welche somit einen polynomialen Speicherverbrauch haben, lösen können, muss dieses Problem ebenfalls in \gls{pspace} liegen. \eqref{eq:pspacer1}
    Probleme in \gls{pspace} müssen also nicht unbedingt effizient ausführbar sein.
    Die Klasse \gls{pspace} hat dadurch einen sehr theoretischen Nutzen.
    Dass \gls{bqp} eine Teilmenge von \gls{pspace} ist, lässt sich mit einer Simulation von Quantencomputern auf klassischen Rechnern begründen, solange diese Simulation polynomialen Speicherbedarf hat. \eqref{eq:pspacer2}
    Solche Simulationen existieren, auch wenn sie eine exponentielle Laufzeit haben.
    Trotzdem reicht die Existenz solcher in der Praxis für größere Eingaben weniger nützlichen Simulationen aus, um $\text{\gls{bqp}} \subseteq \text{\gls{pspace}}$ zu begründen.
    Wir können also zusammenfassen, dass \gls{np} und \gls{bqp}, obwohl sie nicht direkt in einer Relation stehen, beide Teil von \gls{pspace} sind. \cite[121]{homeister2018}

    \begin{figure}[h]
      \centering
      
      \begin{tikzpicture}[node distance = 30pt]

        \tikzstyle{every node}=[font=\scriptsize]
  
        \node[draw, ellipse, minimum width=30pt, minimum height=30pt, anchor=south] at (0,0){$\text{\gls{p}}$};
        \node[draw, ellipse, minimum width=90pt, minimum height=60pt, anchor=south] at (0,0){};
        \node at (0,1.5) {$\text{\gls{np}}$};
        \node[draw, ellipse, minimum width=180pt, minimum height=30pt, anchor=south] at (0,0){};
        \node at (2,0.5) {$\text{\gls{bqp}}$};
        \node[draw, ellipse, minimum width=300pt, minimum height=75pt, anchor=south] at (0,0){};
        \node at (3,1.5) {$\text{\gls{pspace}}$};
      
      \end{tikzpicture}
  
      \caption{Beziehungen von \gls{pspace} zu anderen Komplexitätsklassen}
      \label{fig:sRel}
  
    \end{figure}
